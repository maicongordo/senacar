﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SenaCar.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Descricao : ContentPage
	{
        private const float VIDROS_ELETRICOS = 545;
        private const float TRAVAS_ELETRICAS = 260;
        private const float AR_CONDICIONADO = 480;
        private const float CAMERA_DE_RE = 180;
        private const float CAMBIO = 460;
        private const float SUSPENSAO = 380;
        private const float FREIOS = 245;

        public string TextoVidros_Eletricos
        {
            get
            {
                return string
                    .Format("Vidros Elétricos - R$ {0}", VIDROS_ELETRICOS);
            }
        }

        public string TextoTravas_Eletricas
        {
            get
            {
                return string
                    .Format("Travas Elétricas - R$ {0}", TRAVAS_ELETRICAS);
            }
        }

        public string TextoAr_Condicionado
        {
            get
            {
                return string
                    .Format("Ar Condicionado - R$ {0}", AR_CONDICIONADO);
            }
        }

        public string TextoCamera_de_re
        {
            get
            {
                return string
                    .Format("Câmera de ré - R$ {0}", CAMERA_DE_RE);
            }
        }

        public string TextoCambio
        {
            get
            {
                return string
                    .Format("Câmbio - R$ {0}", CAMBIO);
            }
        }

        public string TextoSuspensao
        {
            get
            {
                return string
                    .Format("Suspensão - R$ {0}", SUSPENSAO);
            }
        }

        public string TextoFreios
        {
            get
            {
                return string
                    .Format("Freios - R$ {0}", FREIOS);
            }
        }

        public string ValorTotal
        { 
            get
            {
                return string.Format("Valor total: R$ {0}",
                                   Servico.Preco
                    +(IncluiVidros_Eletricos ? VIDROS_ELETRICOS : 0)
                    + (IncluiTravas_Eletricas ? TRAVAS_ELETRICAS : 0)
                    + (IncluiAr_Condicionado ? AR_CONDICIONADO : 0)
                    + (IncluiCamera_de_re ? CAMERA_DE_RE : 0)

                    + (IncluiCambio ? CAMBIO : 0)
                    + (IncluiSuspensao ? SUSPENSAO : 0)
                    + (IncluiFreios ? FREIOS : 0)
                    );
            }
        }

        bool incluiVidros_Eletricos;
        public bool IncluiVidros_Eletricos
        {
            get
            {
                return incluiVidros_Eletricos;
            }

            set
            {
                incluiVidros_Eletricos = value;
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiTravas_Eletricas;
        public bool IncluiTravas_Eletricas
        {
            get
            {
                return incluiTravas_Eletricas;
            }

            set
            {
                incluiTravas_Eletricas = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiTransporte)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

           bool incluiAr_Condicionado;
        public bool IncluiAr_Condicionado
        {
            get
            {
                return incluiAr_Condicionado;
            }

            set
            {
                incluiAr_Condicionado = value;
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiCamera_de_re;
        public bool IncluiCamera_de_re
        {
            get
            {
                return incluiCamera_de_re;
            }

            set
            {
                incluiCamera_de_re = value;
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiCambio;
        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }

            set
            {
                incluiCambio = value;
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiSuspensao;
        public bool IncluiSuspensao
        {
            get
            {
                return incluiSuspensao;
            }

            set
            {
                incluiSuspensao = value;
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiFreios;
        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }

            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal));
            }
        }


        public Servico Servico { get; set; }

        public Descricao (Servico servico)
		{
			InitializeComponent ();

           this.Title = servico.Modelo;
           this.Servico = servico;
            this.BindingContext = this;

        }

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Agendamento(this.Servico));

        }
    }
}